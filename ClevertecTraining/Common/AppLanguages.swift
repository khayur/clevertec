//
//  AppLanguages.swift
//  ClevertecTraining
//
//  Created by Yury Khadatovich on 6.02.22.
//

import Foundation

enum AppLanguage: String, CaseIterable, Identifiable {
    
    /// Gives language that matches with language' id. If no matches - returns English as default
    /// - Parameter id: Language' id ("en", "be-BY" etc.)
    /// - Returns: AppLanguage if exists for setted id. If no matches - English.
    static func getLanguageById(_ id: String) -> AppLanguage {
        for language in AppLanguage.allCases {
            if language.id == id {
                return language
            }
        }
        return .english
    }
  
    //MARK: - Cases
    
    case english = "English"
    case russian = "Русский"
    case belarusian = "Беларуская"
    
    //MARK: - Public Properties
    
    var id: String {
        switch self {
            case .english:
                return "en"
            case .russian:
                return "ru"
            case .belarusian:
                return "be-BY"
        }
    }
    
}
