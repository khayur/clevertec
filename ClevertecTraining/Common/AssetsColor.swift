//
//  AssetsColor.swift
//  ClevertecTraining
//
//  Created by Yury Khadatovich on 6.02.22.
//

import UIKit

enum AssetsColor: String {
    case background
    case font
    case logo
    
}


