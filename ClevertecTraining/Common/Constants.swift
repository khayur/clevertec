//
//  Constants.swift
//  ClevertecTraining
//
//  Created by Yury Khadatovich on 6.02.22.
//

import UIKit

struct Constants {
    static let logoImageName = "twotone_water_drop_black_48pt"
    static let appLanguages = AppLanguage.allCases
    static let animationDuration = 0.25
    
}
