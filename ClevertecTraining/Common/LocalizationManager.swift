//
//  AnyLanguage.swift
//  ClevertecTraining
//
//  Created by Yury Khadatovich on 7.02.22.
//

import Foundation

class LocalizationManager {
    
    static let sharedLocalizer = LocalizationManager()
    var appBundle = Bundle.main
    
    func setSelectedLanguage(language: String) {
        guard let languagePath = Bundle.main.path(forResource: language, ofType: "lproj"),
              let receivedBundle = Bundle(path: languagePath)
        else {
            appBundle = Bundle.main
            return
        }
        appBundle = receivedBundle
    }
    
    func stringForKey(key: String) -> String {
        return appBundle.localizedString(forKey: key, value: "", table: nil)
    }
    
}
