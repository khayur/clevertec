//
//  ViewController.swift
//  ClevertecTraining
//
//  Created by Yury Khadatovich on 6.02.22.
//

import UIKit
import SwiftUI

class ViewController: UIViewController {
    
    // MARK: - Public Properties
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if view.overrideUserInterfaceStyle == .dark {
            return .lightContent
        } else {
            return  .darkContent
        }
    }
    
    // MARK: - Private Properties
    
    private let logoImageView = UIImageView()
    private var welcomeLabel = UILabel()
    private let languagePickerParentView = UIView()
    private let darkThemeButton = UIButton()
    private let lightThemeButton = UIButton()
    private let autoThemeButton = UIButton()
    @ObservedObject private var model = Model()
    private var statusBarManager: UIStatusBarManager?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure()
    }
    
    // MARK: - Private Methods
    
    private func configure() {
        view.backgroundColor = UIColor.appColor(.background)
        UserDefaults.standard.synchronize()
        
        configureLogoImageView()
        configureWelcomeLabel()
        configureLanguagePickerParentView()
        configureThemeButtons()
    }
    
    private func configureLogoImageView(){
        let width: CGFloat = self.view.bounds.width / 3
        let height: CGFloat = width
        
        logoImageView.image = UIImage(named: Constants.logoImageName)?.withRenderingMode(.alwaysTemplate)
        logoImageView.tintColor = UIColor.appColor(.logo)
        view.addSubview(logoImageView)
        
        logoImageView.anchor(
            top: view.topAnchor, paddingTop: 50,
            centerX: view.centerXAnchor,
            width: width, height: height
        )
    }
    
    private func configureWelcomeLabel() {
        guard let currentLanguageCode = UserDefaults.standard.string(forKey: "AppleLanguage") else {
            fatalError("Unable to define current localization")
        }
#if DEBUG
        print("Current code from VC: " + currentLanguageCode)
#endif
        let labelString = "WELCOME_LABEL".localized(forLanguageCode: currentLanguageCode, comment: "Welcome label text")
#if DEBUG
        print(labelString + "\n")
#endif
        welcomeLabel.text = labelString
        welcomeLabel.contentMode = .center
        welcomeLabel.numberOfLines = 0
        welcomeLabel.textColor = UIColor.appColor(.font)
        view.addSubview(welcomeLabel)
        
        welcomeLabel.anchor(
            top: logoImageView.bottomAnchor, paddingTop: 20,
            centerX: view.centerXAnchor
        )
    }
    
    private func configureLanguagePickerParentView() {
        languagePickerParentView.backgroundColor = UIColor.appColor(.background)
        
        view.addSubview(languagePickerParentView)
        languagePickerParentView.anchor(
            bottom: view.bottomAnchor,
            centerX: view.centerXAnchor
        )
        
        let picker = LanguagePicker(model: Model())
        self.addSubSwiftUIView(picker, to: languagePickerParentView)
    }
    
    private func configureThemeButtons() {
        let themeButtonsStackView = UIStackView(arrangedSubviews: [darkThemeButton, lightThemeButton, autoThemeButton])
        themeButtonsStackView.axis = .horizontal
        themeButtonsStackView.spacing = 10
        themeButtonsStackView.distribution = .fillEqually
        
        let buttonHeight: CGFloat = 40
        let buttonWidth: CGFloat = 80
        let stackViewWidth = CGFloat(themeButtonsStackView.subviews.count) * buttonWidth + CGFloat(themeButtonsStackView.subviews.count - 1) * themeButtonsStackView.spacing
        
        darkThemeButton.customize(title: "Dark", textColor: .white, backgroundColor: .black, areCornersRounded: true, height: buttonHeight, width: buttonWidth)
        lightThemeButton.customize(title: "Light", textColor: .black, backgroundColor: .white, areCornersRounded: true, height: buttonHeight, width: buttonWidth)
        autoThemeButton.customize(title: "Auto", textColor: .darkGray, backgroundColor: .lightGray, areCornersRounded: true, height: buttonHeight, width: buttonWidth)
        
        darkThemeButton.addTarget(self, action: #selector(self.darkThemeButtonPressed(sender:)), for: .touchUpInside)
        lightThemeButton.addTarget(self, action: #selector(self.lightThemeButtonPressed(sender:)), for: .touchUpInside)
        autoThemeButton.addTarget(self, action: #selector(self.autoThemeButtonPressed(sender:)), for: .touchUpInside)
        view.addSubview(themeButtonsStackView)
        
        themeButtonsStackView.anchor(
            top: welcomeLabel.bottomAnchor, paddingTop: 20,
            centerX: view.centerXAnchor,
            width: stackViewWidth, height: buttonHeight
        )
    }
    
    // MARK: - Actions
    
    @objc func darkThemeButtonPressed(sender: UIButton){
        darkThemeButton.pulsate()
        view.overrideUserInterfaceStyle = .dark
        setNeedsStatusBarAppearanceUpdate()
    }
    
    @objc func lightThemeButtonPressed(sender: UIButton){
        lightThemeButton.pulsate()
        view.overrideUserInterfaceStyle = .light
        setNeedsStatusBarAppearanceUpdate()
    }
    
    @objc func autoThemeButtonPressed(sender: UIButton){
        autoThemeButton.pulsate()
        view.overrideUserInterfaceStyle = .unspecified
        setNeedsStatusBarAppearanceUpdate()
    }
    
}
