//
//  String+Additions.swift
//  ClevertecTraining
//
//  Created by Yury Khadatovich on 7.02.22.
//

import Foundation

extension String {
    func localized(forLanguageCode lanCode: String, comment: String) -> String {
        guard
            let bundlePath = Bundle.main.path(forResource: lanCode, ofType: "lproj"),
            let bundle = Bundle(path: bundlePath)
        else { return "" }
        
        return NSLocalizedString(
            self,
            bundle: bundle,
            value: " ",
            comment: comment
        )
    }
}
