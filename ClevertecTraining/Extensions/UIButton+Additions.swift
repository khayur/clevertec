//
//  UIButton+Additions.swift
//  ClevertecTraining
//
//  Created by Yury Khadatovich on 6.02.22.
//

import UIKit

//MARK: - Button' customize method extended

extension UIButton {
    
    /// Method for customizing UIButton
    /// - Parameters:
    ///   - title: Title for button
    ///   - textColor: Color for button' title text
    ///   - backgroundColor: Background color of button
    ///   - areCornersRounded: "true" if you need rounded corners
    ///   - isBorderNeeded: Adding border of 'textColor" color, 'true' by default
    ///   - height: Height of the button, '30' by default
    ///   - width: Width of the button, '100' by default
    func customize(
        title: String,
        textColor: UIColor,
        backgroundColor: UIColor,
        areCornersRounded: Bool,
        isBorderNeeded: Bool = true,
        height: CGFloat = 30,
        width: CGFloat = 100
    ) {
        
        self.setTitle(title, for: .normal)
        self.setTitleColor(textColor, for: .normal)
        self.backgroundColor = backgroundColor
        self.frame.size.height = height
        self.frame.size.width = width
        
        if areCornersRounded {
            self.clipsToBounds = true
            self.layer.cornerRadius = height / 2
        }
        
        if isBorderNeeded {
            self.layer.borderWidth = 0.5
            guard let colorComponents = textColor.cgColor.components else { return }
            if colorComponents.count == 2 {
                self.layer.borderColor = CGColor(gray: colorComponents[0], alpha: colorComponents[1])
            } else if colorComponents.count == 4 {
                self.layer.borderColor = CGColor(red: colorComponents[0],
                                                 green: colorComponents[1],
                                                 blue: colorComponents[2],
                                                 alpha: colorComponents[3]
                )
            }
        }
    }
    
}

//MARK: - Button' pulsate method extended

extension UIButton {
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = Constants.animationDuration
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        layer.add(pulse, forKey: nil)
    }
    
}

