//
//  UIColor+Additions.swift
//  ClevertecTraining
//
//  Created by Yury Khadatovich on 6.02.22.
//

import UIKit

//MARK: - appColor method extended

extension UIColor {
    
    /// Gives color of UIColor type for requested color from application Assets Colors
    /// - Parameter name: name of color in Color' Assets
    /// - Returns: UIColor, defined for requested name
    static func appColor(_ name: AssetsColor) -> UIColor? {
        return UIColor(named: name.rawValue)
    }
    
}
