//
//  UIViewController+Additions.swift
//  ClevertecTraining
//
//  Created by Yury Khadatovich on 6.02.22.
//

import UIKit
import SwiftUI

//MARK: - Adding sub-swiftUI view method extended

extension UIViewController {
    
    /// Add a SwiftUI `View` as a child of the input `UIView`.
    /// - Parameters:
    ///   - swiftUIView: The SwiftUI `View` to add as a child.
    ///   - view: The `UIView` instance to which the view should be added.
    func addSubSwiftUIView<Content>(_ swiftUIView: Content, to view: UIView) where Content : View {
        let hostingController = UIHostingController(rootView: swiftUIView)
        hostingController.view.backgroundColor = .clear
        
        addChild(hostingController)
        view.addSubview(hostingController.view)
        
        hostingController.view.translatesAutoresizingMaskIntoConstraints = false
        hostingController.view.anchor(top: view.topAnchor, leading: view.leadingAnchor)
        view.anchor(bottom: hostingController.view.bottomAnchor, trailing: hostingController.view.trailingAnchor)
        
        hostingController.didMove(toParent: self)
    }
}
