//
//  Model.swift
//  ClevertecTraining
//
//  Created by Yury Khadatovich on 6.02.22.
//

import SwiftUI

final class Model: ObservableObject {
    
    //MARK: - Public Properties
    
    @Published var selectedLanguage: String = UserDefaults.standard.string(forKey: "AppleLanguage") ?? "en" {
        willSet(newValue){
            
#if DEBUG
            print("New Value: \(newValue)")
            print("Selected: " + selectedLanguage)
#endif
            UserDefaults.standard.set(newValue, forKey: "AppleLanguage")
            UserDefaults.standard.synchronize()
            Bundle.swizzleLocalization()
            
        }
    }
    
}
