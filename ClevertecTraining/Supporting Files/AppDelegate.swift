//
//  AppDelegate.swift
//  ClevertecTraining
//
//  Created by Yury Khadatovich on 6.02.22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    //MARK: -Public Properties
    var currentLanguage: String {
        UserDefaults.standard.synchronize()
        
        guard let language = UserDefaults.standard.object(forKey: "AppleLanguage") as? String else {
            return Model().selectedLanguage
        }
        
        return language
    }
    
    //MARK: - Public Methods
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UserDefaults.standard.set(currentLanguage, forKey: "AppleLanguage")
        Bundle.swizzleLocalization()
        return true
    }
    
}
