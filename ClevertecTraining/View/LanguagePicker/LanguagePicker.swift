//
//  LanguagePicker.swift
//  ClevertecTraining
//
//  Created by Yury Khadatovich on 6.02.22.
//

import SwiftUI

struct LanguagePicker: View {
    @ObservedObject var model = Model()
    
    var body: some View {
        Picker(selection: $model.selectedLanguage, label: Text("Language")) {
            ForEach(AppLanguage.allCases) {
                Text($0.rawValue).tag($0)
            }
        }
    }
    
}
